
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>View Transaction</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        .wrapper{
            width: 500px;
            margin: 0 auto;
        }
    </style>
</head>
<body>
<div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header clearfix">
                        <h2 class="pull-left">View Transaction</h2>
                        <a href="index.php" class="btn btn-success pull-right">View Dashboard</a>
                    </div>
                    <?php
                        $id= $_REQUEST['id'];
                        include 'config.php';
                        $delete="select * from transactions where id=$id";
                        $query=mysqli_query($con, $delete);
                        while($row=mysqli_fetch_array($query)){
                            //echo $row[4];
                    ?>
                    
                    <form action="" method="" >
                    <fieldset disabled>
                        <div class="form-group">
                            <label>Title *</label>
                            <input type="text" name="title" class="form-control" value="<?php echo $row[1] ?>" >
                        </div>
                        
                        <div class="form-group">
                            <label>Description</label>
                            <textarea name="description" class="form-control"> <?php echo $row[2] ?> </textarea>
                        </div>
                        
                        <div class="form-group ">
                            <label>Amount *</label>
                            <input type="text" name="amount" class="form-control" value="<?php echo $row[3] ?>" >

                        </div>
                        
                        <div class="form-group ">
                            <label>Purchase Date *</label>
                            <input type="date" name="purchased_date" class="form-control" value="<?php echo $row[4] ?>">

                        </div>
                        
                        <div class="form-group ">
                            <label>Purchased By *</label>
                            <input type="email" name="purchased_by" class="form-control" value="<?php echo $row[5] ?>">
                        </div>
                       </fieldset> 
                    </form>
                    
                    <?php
                        }

                    ?>

                </div>
            </div>        
        </div>
    </div>
</body>
</html>