<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dashboard</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js"></script>
    <style type="text/css">
        .wrapper{
            width: 650px;
            margin: 0 auto;
        }
        .page-header h2{
            margin-top: 0;
        }
        table tr td:last-child a{
            margin-right: 15px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();   
        });
    </script>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header clearfix">
                        <h2 class="pull-left">Transaction Details</h2>
                        <a href="create.php" class="btn btn-success pull-right">Add New Transaction</a>
                    </div>
                    <table class='table table-bordered table-striped'>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Amount in AUD</th>
                                <th>Purchased Date</th>
                                <th>Purchased By</th>
                                <th colspan="3">Action</th>
                            </tr>
                            
                        </thead>
                        <tbody>
                            <?php
                                include 'config.php';
                                $select="select * from transactions";
                                $query=mysqli_query($con, $select);
                                while ($row=mysqli_fetch_array($query)){
                                  echo "<tr>
                                        <td>$row[0]</td>
                                        <td>$row[1]</td>
                                        <td>$$row[3]</td>
                                        <td>$row[4]</td>
                                        <td>$row[5]</td>
                                        <td><a href='view.php?id=$row[0]' data-toggle='tooltip' data-placement='top' title='View' ><span class='glyphicon glyphicon-eye-open'></span></a></td>
                                        <td><a href='update.php?id=$row[0]'  data-toggle='tooltip' data-placement='top'  title='Edit'><span class='glyphicon glyphicon-pencil'></span> </a></td>
                                        <td><a href='delete.php?id=$row[0]'  data-toggle='tooltip' data-placement='top'  title='Delete'><span class='glyphicon glyphicon-trash'></span></a></td>
                                        </tr>
                                    ";
                                }
                            ?>
                            
                        </tbody>
                    </table>

                </div>
            </div>        
        </div>
    </div>
</body>
</html>