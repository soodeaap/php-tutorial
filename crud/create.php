<?php
    $name_err=""; $amount_err=""; $date_err=""; $by_err="";

    //if($a=="") <--> if(empty($a))

    //code runs only if there is post method attached on the form after submit button is clicked
    if($_SERVER["REQUEST_METHOD"] == "POST"){ // if(isset($_POST['title']) && isset($_POST['amount']) && isset($_POST['pruchased_date']) && isset($_POST['purchased_by']))
       

        $title=trim($_POST['title']);
        $desc=trim($_POST['description']);
        $amount=trim($_POST['amount']);
        $date=trim($_POST['purchased_date']);
        $by=trim($_POST['purchased_by']);


        //code for server side valdiation START
        if(empty($title)){
            $name_err="Title can't be empty";
        }
        
    
        if (filter_var($amount, FILTER_VALIDATE_FLOAT) === false){
            $amount_err="Amount must be number";
        }
        else{
            if($amount<=0 ){
                $amount_err="Amount must be greater than 0";
            }
        }
        

        if( empty($date) ){
            $date_err="Date can't be empty";
        }


        if(empty($by) ){
            $by_err="Email should be provided";
        }
        //code for server side valdiation END

       

    }
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create Transaction</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
    <style type="text/css">
        .wrapper{
            width: 500px;
            margin: 0 auto;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header clearfix">
                        <h2 class="pull-left">New Transaction</h2>
                        <a href="index.php" class="btn btn-success pull-right">View Dashboard</a>
                    </div>
                    
                    <p>Please fill this form. Field with * are compulsory</p>
                    <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
                        <div class="form-group <?php echo (!empty($name_err)) ? 'has-error' : ''; ?>">
                            <label>Title *</label>
                            <input type="text" name="title" class="form-control" value="">
                            <span class="help-block"><?php echo $name_err;?></span>
                        </div>
                        
                        <div class="form-group">
                            <label>Description</label>
                            <textarea name="description" class="form-control"></textarea>
                        </div>
                        
                        <div class="form-group <?php echo (!empty($amount_err)) ? 'has-error' : ''; ?>">
                            <label>Amount *</label>
                            <input type="text" name="amount" class="form-control" value="" >
                            <span class="help-block"><?php echo $amount_err;?></span>
                        </div>
                        
                        <div class="form-group <?php echo (!empty($date_err)) ? 'has-error' : ''; ?>">
                            <label>Purchase Date *</label>
                            <input type="date" name="purchased_date" class="form-control" value="">
                            <span class="help-block"><?php echo $date_err;?></span>
                        </div>
                        
                        <div class="form-group <?php echo (!empty($by_err)) ? 'has-error' : ''; ?>">
                            <label>Purchased By *</label>
                            <input type="email" name="purchased_by" class="form-control" value="">
                            <span class="help-block"><?php echo $by_err;?></span>
                        </div>
                        
                        <input type="submit" class="btn btn-primary" value="Submit">
                        <a href="index.php" class="btn btn-default">Cancel</a>
                    </form>
                </div>
            </div>        
        </div>
    </div>
</body>
</html>
<?php
if($_SERVER["REQUEST_METHOD"] == "POST"){

    if(empty($name_err) && empty($amount_err)  && empty($by_err) && empty($date_err) ){
        //code for insertion
        include 'config.php';
        $query="Insert into transactions(title, description, amount, purchased_date, purchased_by) values ('$title','$desc','$amount','$date','$by') ";
        
        $result= mysqli_query($con, $query);
        if($result){
            echo "<script type='text/javascript'>toastr.success('Successfully Saved')</script>";
           
        }
        else{
            header("location: error.php");
            exit();
        }

        mysqli_close($con);        
    }
}
    
?>