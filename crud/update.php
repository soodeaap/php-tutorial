
<?php
//suru page load huda
    if($_SERVER["REQUEST_METHOD"] == "GET"){
        $id= $_REQUEST['id'];
        include 'config.php';
        $delete="select * from transactions where id=$id";
        $query=mysqli_query($con, $delete);
        while($row=mysqli_fetch_array($query)){
            $id=trim($row[0]);
            $title=trim($row[1]);
            $desc=trim($row[2]);
            $amount=trim($row[3]);
            $date=trim($row[4]);
            $by=trim($row[5]);

        }
    }

        //echo $row[4];
?>


<?php
//update button click garepaxci
    $name_err=""; $amount_err=""; $date_err=""; $by_err="";
    
    if($_SERVER["REQUEST_METHOD"] == "POST"){ // if(isset($_POST['title']) && isset($_POST['amount']) && isset($_POST['pruchased_date']) && isset($_POST['purchased_by']))
       $id=trim($_POST['id']);
        $title=trim($_POST['title']);
        $desc=trim($_POST['description']);
        $amount=trim($_POST['amount']);
        $date=trim($_POST['purchased_date']);
        $by=trim($_POST['purchased_by']);


        //code for server side valdiation START
        if(empty($title)){
            $name_err="Title can't be empty";
        }
        
    
        if (filter_var($amount, FILTER_VALIDATE_FLOAT) === false){
            $amount_err="Amount must be number";
        }
        else{
            if($amount<=0 ){
                $amount_err="Amount must be greater than 0";
            }
        }
        
        if( empty($date) ){
            $date_err="Date can't be empty";
        }


        if(empty($by) ){
            $by_err="Email should be provided";
        }
        //code for server side valdiation END
        include 'config.php';
       $qry="update transactions set title='$title', description='$desc', amount='$amount', purchased_date='$date', purchased_by='$by'  where id='$id' ";
       $query=mysqli_query($con, $qry);
       if($query){
            echo "Updated";
            header("location: index.php");
            exit();
       }

       else{
        header("location: error.php");
        exit();
       }

    }
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>View Transaction</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        .wrapper{
            width: 500px;
            margin: 0 auto;
        }
    </style>
</head>
<body>
<div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header clearfix">
                        <h2 class="pull-left">Update Transaction</h2>
                        <a href="index.php" class="btn btn-success pull-right">View Dashboard</a>
                    </div>
                    
                    
                    <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
                    <input type="hidden" name="id" value="<?php echo $id; ?>"/>
                    <div class="form-group <?php echo (!empty($name_err)) ? 'has-error' : ''; ?>">
                            <label>Title *</label>
                            <input type="text" name="title" class="form-control" value="<?php echo $title ?>" >
                            <span class="help-block"><?php echo $name_err;?></span>
                        </div>
                        
                        <div class="form-group">
                            <label>Description</label>
                            <textarea name="description" class="form-control"><?php echo $desc ?></textarea>
                        </div>
                        
                        <div class="form-group <?php echo (!empty($amount_err)) ? 'has-error' : ''; ?>">
                            <label>Amount *</label>
                            <input type="text" name="amount" class="form-control"value="<?php echo $amount ?>" >
                            <span class="help-block"><?php echo $amount_err;?></span>
                        </div>
                        
                        <div class="form-group <?php echo (!empty($date_err)) ? 'has-error' : ''; ?>">
                            <label>Purchase Date *</label>
                            <input type="date" name="purchased_date" class="form-control" value="<?php echo $date ?>">
                            <span class="help-block"><?php echo $date_err;?></span>
                        </div>
                        
                        <div class="form-group <?php echo (!empty($by_err)) ? 'has-error' : ''; ?>">
                            <label>Purchased By *</label>
                            <input type="email" name="purchased_by" class="form-control" value="<?php echo $by ?>">
                            <span class="help-block"><?php echo $by_err;?></span>
                        </div>

                        <input type="submit" class="btn btn-primary" value="Update">
                        <a href="index.php" class="btn btn-default">Cancel</a>
                    </form>
                    

                </div>
            </div>        
        </div>
    </div>
</body>
</html>